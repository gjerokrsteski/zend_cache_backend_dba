<?php
error_reporting(E_ALL | E_STRICT);

ini_set('display_errors', 1);

date_default_timezone_set('Europe/Berlin');

set_include_path(
    dirname(dirname(__FILE__))
    .'/zf-cache-backend-dba'
    .PATH_SEPARATOR
    .dirname(__FILE__)
    .PATH_SEPARATOR
    .get_include_path()
);

spl_autoload_register(
   function($class) {
      $classes = null;
      if ($classes === null)
      {
         $classes = array(
            'zend_cache_backend_dba' => '/Zend/Cache/Backend/Dba.php',
            'zend_cache_backend_interface' => '/Zend/Cache/Backend/Interface.php',
            'zend_cache_backend' => '/Zend/Cache/Backend.php',
            'zend_cache_core' => '/Zend/Cache/Core.php',
            'zend_cache_exception' => '/Zend/Cache/Exception.php',
            'zend_exception' => '/Zend/Exception.php',
            'zend_cache' => '/Zend/Cache.php',
            'zend_cache_manager' => '/Zend/Cache/Manager.php',
            'zend_cache_backend' => '/Zend/Cache/Backend.php',
            'zend_cache_backend_extendedinterface' => '/Zend/Cache/Backend/ExtendedInterface.php'
          );
      }
      $cn = strtolower($class);
      if (isset($classes[$cn]))
      {
         require __DIR__ . $classes[$cn];
      }
   }
);