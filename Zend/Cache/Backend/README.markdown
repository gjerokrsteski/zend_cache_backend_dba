Introduction
============

    Zend_Cache_Backend_Dba is an additional component for the zend framework.
	It uses the database (dbm-style) abstraction layer for backend caching.
	Suported dba-drivers are db4 and flatfile as default. 

Nice to know
----------------------------------------------------------

    Not all DBA-style databases can replace key-value pairs, like the CDB. The CDB database
    can handle only with fixed key-value pairs. The best and fastest handlers for DBA-style-caching
    are: QDBM, Berkeley DB (DB4), NDBM and least the Flatfile.


Samples for the configuration
----------------------------------------------------------

    <?php
    // Oracle Berkeley DB 4 with persistent connection
    $config = array(
        'driver'  => 'Dba',
        'options' => array(
            'cache_dir' => '/path/to/your/directory/',
            'file_name' => 'your-cache-filename.db4',
            'persistently' => true,
            'dba_handler' => 'db4',
        )
    );
    
    // If no dba-driver installed, than use the default configuration.
    $config = array(
        'driver'  => 'Dba',
        'options' => array(
            'file_name' => 'your-cache-filename.flat',
        )
    );
    
    $cache = new Zend_Cache_Backend($config);
    ...
    ..
    .

    ?>
