<?php
class Zend_Cache_Backend_Dba
extends Zend_Cache_Backend
implements Zend_Cache_Backend_ExtendedInterface
{
    /**
     * @var resource
     */
    protected $_dba;

    /**
     * @var resource
     */
    protected $_handler;

    /**
     * Default configuration is flatfile if no dba-driver is installed.
     *
     * Not all of the DBA-style databases can replace key-value pairs,
     * like the CDB. The CDB database can handle only with fixed
     * key-value pairs. The best and fastest handlers for DBA-style
     * caching are: QDBM, Berkeley DB (DB4), NDBM and least the Flatfile.
     *
     * @var array
     */
    protected $_options = array(
        'cache_dir'    => null,
        'file_name'    => null,
        'dba_handler'  => 'flatfile',
        'dba_mode'     => 'c',
        'persistently' => false,
    );

    /**
     * Constructor
     *
     * @param  array $options associative array of options
     * @throws Zend_Cache_Exception
     * @return void
     */
    public function __construct(array $options = array())
    {
        parent::__construct($options);

        if ($this->_options['cache_dir'] !== null)
        {
            $this->setCacheDir($this->_options['cache_dir']);
        }
        else
        {
            $this->setCacheDir(self::getTmpDir() . DIRECTORY_SEPARATOR, false);
        }

        $file = $this->_options['cache_dir'].$this->_options['file_name'];

        $this->_retrieveDbaConnection(
            $file,
            $this->_options['dba_mode'],
            $this->_options['dba_handler'],
            $this->_options['persistently']
        );
    }

    /**
     * @param string $file the cache-file.
     *
     * @param string $mode For read/write access.
     *  r = read access
     *  w = read/write access to an already existing database
     *  c = for read/write access and database creation if it doesn't currently exist
     *  n = for create, truncate and read/write access
     *
     * @param string $handler the dba handler.
     * You have to install one of this handlers before use.
     *
     * db4      = Oracle Berkeley DB 4   - for reading and writing.
     * qdbm     = Quick Database Manager - for reading and writing.
     * gdbm     = GNU Database Manager   - for reading and writing.
     * flatfile = default dba extension  - for reading and writing.
     *
     * Use flatfile-handler only when you cannot install one,
     * of the libraries required by the other handlers,
     * and when you cannot use bundled cdb handler.
     *
     * To enable support for the handlers
     * @see http://www.php.net/manual/en/dba.installation.php
     *
     * DBA supportet handlers:
     * @see http://www.php.net/manual/en/function.dba-handlers.php
     *
     * @param booelan $persistently
     * @throws Zend_Cache_Exception
     * @return void
     */
    protected function _retrieveDbaConnection($file, $mode = 'c', $handler = 'flatfile', $persistently = true)
    {
        if (false === extension_loaded('dba'))
        {
            Zend_Cache::throwException(
                'The DBA extension is required for this wrapper,'.
                ' but the extension is not loaded'
            );
        }

        if (false === in_array($handler, dba_handlers(false)))
        {
            Zend_Cache::throwException(
                'The '.$handler.' handler is required for the DBA extension,'.
                ' but the handler is not installed'
            );
        }

        $this->_dba = (true === $persistently)
                        ? dba_popen($file, $mode, $handler)
                        : dba_open($file, $mode, $handler);

        $this->_handler = $handler;
    }

    /**
     * Return an associative array of capabilities (booleans) of the backend
     *
     * The array must include these keys :
     * - automatic_cleaning (is automating cleaning necessary)
     * - tags (are tags supported)
     * - expired_read (is it possible to read expired cache records
     *                 (for doNotTestCacheValidity option for example))
     * - priority does the backend deal with priority when saving
     * - infinite_lifetime (is infinite lifetime can work with this backend)
     * - get_list (is it possible to get the list of cache ids and the complete list of tags)
     *
     * @return array associative of with capabilities
     */
    public function getCapabilities()
    {
        return array(
            'automatic_cleaning' => true,
            'tags'               => false,
            'expired_read'       => false,
            'priority'           => false,
            'infinite_lifetime'  => true,
            'get_list'           => true
        );
    }

    /**
     * Set the cache_dir (particular case of setOption() method)
     *
     * @param  string  $value
     * @param  boolean $trailingSeparator If true, add a trailing separator is necessary
     * @throws Zend_Cache_Exception
     * @return void
     */
    public function setCacheDir($value, $trailingSeparator = true)
    {
        if (false === is_dir($value))
        {
            Zend_Cache::throwException('cache_dir must be a directory '.$value);
        }

        if (false === is_writable($value))
        {
            Zend_Cache::throwException('cache_dir is not writable '.$value);
        }

        if (true === $trailingSeparator)
        {
            $value = rtrim(realpath($value), '\\/') . DIRECTORY_SEPARATOR;
        }

        $this->_options['cache_dir'] = $value;
    }

    /**
     * Set the frontend directives
     *
     * @param array $directives assoc of directives
     */
    public function setDirectives($directives)
    {
        parent::setDirectives($directives);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else)
     *
     * Note : return value is always "string" (unserialization is done by the core not by the backend)
     *
     * @param  string  $id                     Cache id
     * @param  boolean $doNotTestCacheValidity If set to true, the cache validity won't be tested
     * @return string|false cached data
     */
    public function load($id, $doNotTestCacheValidity = false)
    {
        if (true === $doNotTestCacheValidity)
        {
            return $this->_has($id);
        }

        return $this->_get($id);
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param  string $id cache id
     * @return mixed|false (a cache is not available) or "last modified" timestamp (int) of the available cache record
     */
    public function test($id)
    {
        $res = $this->_getObject($id);
        
        if (true === is_object($res))
        {
        	return $res->mtime;
        }
        
        return false;
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is always "string" (serialization is done by the
     * core not by the backend)
     *
     * @param  string $data            Datas to cache
     * @param  string $id              Cache id
     * @param  array $tags             Array of strings, the cache record will be tagged by each string entry
     * @param  int   $specificLifetime If != false, set a specific lifetime for this cache record (null => infinite lifetime)
     * @return boolean true if no problem
     */
    public function save($data, $id, $tags = array(), $specificLifetime = false)
    {
        return $this->_put($id, $data, $specificLifetime);
    }

    /**
     * Remove a cache record
     *
     * @param  string $id Cache id
     * @return boolean True if no problem
     */
    public function remove($id)
    {
        return $this->_delete($id);
    }

    /**
     * Clean some cache records
     *
     * Available modes are :
     * Zend_Cache::CLEANING_MODE_ALL (default)    => remove all cache entries ($tags is not used)
     * Zend_Cache::CLEANING_MODE_OLD              => remove too old cache entries ($tags is not used)
     *
     * @param  string $mode Clean mode
     * @param  array  $tags Array of tags
     * @return boolean false if problem
     */
    public function clean($mode = Zend_Cache::CLEANING_MODE_ALL, $tags = array())
    {
        if ($mode == Zend_Cache::CLEANING_MODE_ALL)
        {
            return $this->_clean();
        }

        if ($mode == Zend_Cache::CLEANING_MODE_OLD)
        {
            return $this->_clean(false, microtime(true));
        }

        return false;
    }

    /**
     * Return an array of stored cache ids
     *
     * @return array array of stored cache ids (string)
     */
    public function getIds()
    {
        $ids = array();
        $dba = $this->getDba();
        $pointer = dba_firstkey($dba);

        while($pointer)
        {
            $ids[] = $pointer;
            $pointer = dba_nextkey($dba);
        }

        return $ids;
    }

    /**
     * Return an array of stored tags
     *
     * @return array array of stored tags (string)
     */
    public function getTags()
    {
        return array();
    }

    /**
     * Return an array of stored cache ids which match given tags
     *
     * In case of multiple tags, a logical AND is made between tags
     *
     * @param array $tags array of tags
     * @return array array of matching cache ids (string)
     */
    public function getIdsMatchingTags($tags = array())
    {
        return array();
    }

    /**
     * Return an array of stored cache ids which don't match given tags
     *
     * In case of multiple tags, a logical OR is made between tags
     *
     * @param array $tags array of tags
     * @return array array of not matching cache ids (string)
     */
    public function getIdsNotMatchingTags($tags = array())
    {
        return array();
    }

    /**
     * Return an array of stored cache ids which match any given tags
     *
     * In case of multiple tags, a logical AND is made between tags
     *
     * @param array $tags array of tags
     * @return array array of any matching cache ids (string)
     */
    public function getIdsMatchingAnyTags($tags = array())
    {
        return array();
    }

    /**
     * Return the filling percentage of the backend storage
     * @throws Zend_Cache_Exception
     * @return int integer between 0 and 100
     */
    public function getFillingPercentage()
    {
        clearstatcache();

        $free = disk_free_space($this->_options['cache_dir']);
        $total = disk_total_space($this->_options['cache_dir']);

        if ($total == 0)
        {
            Zend_Cache::throwException('can\'t get disk_total_space');
        }
        else
        {
            if ($free >= $total)
            {
                return 100;
            }

            return ((int) (100. * ($total - $free) / $total));
        }
    }

    /**
     * Return an array of metadatas for the given cache id
     *
     * The array must include these keys :
     * - expire : the expire timestamp
     * - tags : a string array of tags
     * - mtime : timestamp of last modification time
     *
     * @param string $id cache id
     * @return array array of metadatas (false if the cache id is not found)
     */
    public function getMetadatas($id)
    {
    	$res = $this->_getObject($id);
    	
    	if (true === is_object($res))
    	{    	
	    	return array(
	    		'expire' => $res->mtime + $res->ltime,
	    		'mtime'  => $res->mtime,
	    	);
    	}
    	
    	return false;
    }

    /**
     * Give (if possible) an extra lifetime to the given cache id
     *
     * @param string $id cache id
     * @param int $extraLifetime
     * @return boolean true if ok
     */
    public function touch($id, $extraLifetime)
    {
        return false;
    }


    /**
     * @param string $identifier
     * @param object $object
     * @param integer|false $ltime
     * @return boolean
     */
    protected function _put($identifier, $object, $ltime)
    {
        $ret = @dba_replace(
          $identifier,
          $this->_serialize($object, $ltime),
          $this->_dba
        );

        if (!$ret)
        {
            $err = error_get_last();

            throw new RuntimeException(
            	"dba_replace failed: {$err['message']}"
            );
        }

        return true;
    }

    /**
     * @param string $identifier
     * @return object | false
     */
    protected function _get($identifier)
    {
        $res = $this->_getObject($identifier);

        if (false === $res)
        {
            return false;
        }

        if (false === $res->ltime 
        || (microtime(true) - $res->mtime) < $res->ltime)
        {
            return $res->object;
        }

        $this->_delete($identifier);

        return false;
    }
    
    /**
     * @param string $identifier
     * @return object | false
     */
    protected function _getObject($identifier)
    {
    	$fetchObject = dba_fetch($identifier, $this->_dba);

        if (false === $fetchObject)
        {
            return false;
        }

        return $this->_unserialize($fetchObject);
    }

    /**
     * @param string $identifier
     * @return boolean
     */
    protected function _delete($identifier)
    {
        if (false === is_resource($this->_dba))
        {
            return false;
        }

        return dba_delete($identifier, $this->_dba);
    }

    /**
     * @param string $identifier
     * @return boolean
     */
    protected function _has($identifier)
    {
        $res = dba_fetch($identifier, $this->_dba);
        
        return (false === $res) ? false : true;
    }

    /**
     * Close the handler.
     */
    protected function _closeDba()
    {
        dba_close($this->_dba);
    }

    /**
     * @return resource
     */
    public function getDba()
    {
        return $this->_dba;
    }

    protected function _mask($item)
    {
        return (object) $item;
    }

    protected function _unmask($item)
    {
        if (isset($item->scalar))
        {
            return $item->scalar;
        }

        return (array) $item;
    }

    /**
     * Serialize the object as stdClass.
     * @return string containing a byte-stream representation.
     */
    protected function _serialize($object, $ltime)
    {
        $masked = false;

        if (false === is_object($object))
        {
            $object = $this->_mask($object);
            $masked = true;
        }

        $objectInformation         = new stdClass();
        $objectInformation->type   = get_class($object);
        $objectInformation->object = $object;
        $objectInformation->fake   = $masked;
        $objectInformation->mtime  = microtime(true);
        $objectInformation->ltime  = $ltime;

        if ($object instanceof SimpleXMLElement)
        {
            $objectInformation->object = $object->asXml();
        }

        return serialize($objectInformation);
    }

    /**
     * Unserialize the object.
     * @return stdClass
     */
    protected function _unserialize($object)
    {
        $objectInformation = unserialize($object);

        if (true === $objectInformation->fake)
        {
            $objectInformation->object = $this->_unmask($objectInformation->object);
        }

        if ($objectInformation->type == 'SimpleXMLElement')
        {
            $objectInformation->object = simplexml_load_string($objectInformation->object);
        }

        return $objectInformation;
    }

    /**
     * The internal cleaning process.
     * @param boolean $cleanAll
     * @param integer $microtime as microseconds
     * @return void
     */
    protected function _clean($cleanAll = true, $microtime = 300)
    {
        $pointer = dba_firstkey($this->getDba());

        while($pointer)
        {
            if (true === $cleanAll)
            {
                $this->_delete($pointer);
            }
            else
            {
                $this->_get($pointer, $microtime);
            }

            $pointer = dba_nextkey($this->getDba());
        }

        dba_optimize($this->getDba());
    }
}