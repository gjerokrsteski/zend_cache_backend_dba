<?php
class Zend_Cache_DbaBackendTest extends PHPUnit_Framework_TestCase
{
    public function testBackendConstructorCorrectCall()
    {
        // If no dba-driver installed, than use the default configuration.
        $config = array(
            'driver'  => 'Dba',
            'options' => array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
            )
        );

        $cache = new Zend_Cache_Backend($config);
        
        $this->assertInstanceOf('Zend_Cache_Backend', $cache);
    }
    
    public function testBackendDbaConstructorCorrectCall()
    {
        // If no dba-driver installed, than use the default configuration.
        $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
         );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $this->assertInstanceOf('Zend_Cache_Backend_Dba', $cache);
    }
    
    /**
     * @expectedException Zend_Cache_Exception
     */
    public function testCreatingNewInstanceThrowingExcetionCachePathNotADirecotory()
    {
    	 $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp-xxx/',
         );

        $cache = new Zend_Cache_Backend_Dba($config);
    }
    
    public function testSaveAndLoadSomeObjectData()
    {
    	$id = md5(uniqid(rand(), true));
    	
    	$data = new stdClass();
        $data->name = 'Joe';
        $data->phone = '123456';
        
        $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $res = $cache->save($data, $id);
        
        $this->assertTrue($res);
           
        $this->assertEquals($data, $cache->load($id));
    }
    
    public function testSaveAndLoadStringDataAndTrieToReplaceItAndDeleteIt()
    {

    	$id = md5(uniqid(rand(), true));
    	
    	$data = 'Joe Rocker 1979';
        
        $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $res = $cache->save($data, $id);
        
        $this->assertTrue($res);
        $this->assertEquals($data, $cache->load($id));
        
        $data = 'Max Man 1980';
        $res = $cache->save($data, $id);
        
        $this->assertTrue($res);
        $this->assertEquals($data, $cache->load($id));
        
        $res = $cache->remove($id);
        
        $this->assertTrue($res);
   }
   
    public function testHandlingWithSimpleXMLElement()
    {
        $identifier = md5(uniqid());

        $string = "<?xml version='1.0'?>
        <document>
         <title>Let us cache</title>
         <from>Joe</from>
         <to>Jane</to>
         <body>Some content here</body>
        </document>";

        $simplexml = simplexml_load_string(
            $string,
            'SimpleXMLElement',
            LIBXML_NOERROR|LIBXML_NOWARNING|LIBXML_NONET
        );

         $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $cache->save($simplexml, $identifier);
        
        $object_from_cache = $cache->load($identifier);

        $this->assertEquals($simplexml->asXML(), $object_from_cache->asXML());
    }
    
    public function testGetAllIdsAndCleanAllIds()
    {
        $config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $cache->clean();
        
        $this->assertEmpty($cache->getIds());
        
        $id1 = md5(uniqid(rand(), true));
        $id2 = md5(uniqid(rand(), true));
        
        $cache->save('abc', $id1);
        $cache->save('def', $id2);
        
        $this->assertEquals(2, count($cache->getIds()));
    }
    
    public function testLoadingDataAfterTheSpecifiedLifetime()
    {
    	$id = md5(uniqid(rand(), true));
    	
    	$config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $cache->save(array('some-data-here'), $id, $tags = array(), 1);
        
        sleep(2);
        
        $this->assertTrue($cache->load($id, $doNotTestCacheValidity = true));     
        $this->assertFalse($cache->load($id));
    }
    
    public function testMethodTestExpectingNoBoolean()
    {
    	$id = md5(uniqid(rand(), true));
    	
    	$config = array(
        		'cache_dir' => dirname(__FILE__).'/tmp/',
                'file_name' => 'dba-backend-cache.flat',
                'dba_mode'  => 'c',
        );

        $cache = new Zend_Cache_Backend_Dba($config);
        
        $cache->save(array('some-data-here'), $id);
        
        $this->assertTrue((false !== $cache->test($id)));
    }
}
